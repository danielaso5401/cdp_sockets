import socket
import threading
import sys
import pickle
import numpy as np
from concurrent.futures import ThreadPoolExecutor
import MultiplicacionHilo

class Servidor():
	"""Al iniciar ya establecemos el host y el puerto a donde los clientes se conectarán 
	y así mismmo el puerto por donde se realizará la conexión.
	"""
	def __init__(self, host="localhost", port=6666):
		"""Aquí es donde almacenaremos las conexiones de los clientes aceptadas"""
		self.clientes = []
		"""Sockets
		Aquí creamos el socket para el servidor (Receptor).
		"""
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		"""Asignamos el host y puerto por el que se van a recibir las solicitud de los clientes."""
		self.sock.bind((str(host), int(port)))
		"""Establecemos un límite de conexiones a escuchar. En este caso le pusimos 10."""
		self.sock.listen(10)
		self.sock.setblocking(False)

		"""Ahora creamos dos hilos, uno para acptar las conexiones de los clientes
		y otro para el procesamiento de solicitudes para la Multiplicación de matrices.
		"""

		aceptar = threading.Thread(target=self.aceptarCon)
		procesar = threading.Thread(target=self.procesarCon)

		"""Indicamos que los hilos sean demonios para que estos sean ejecutados en segundo plano
		a la espera de aceptar conexiones y procesar cálculos (multiplicación de matrices recibidas).
		#Así mismo inicializamos cada uno de estos (aceptar.start() y procesar.start()).
		"""

		aceptar.daemon = True
		aceptar.start()

		procesar.daemon = True
		procesar.start()

		"""Este bucle es con el fin de mantener al servidor aún activo
		Hasta que igresemos la palabra salir.
		"""

		while True:
			opcion = input('->')
			if opcion == 'salir':
				self.sock.close()
				sys.exit()
			else:
				pass

	"""Esta función es utilizada para devolver la matriz respuesta de la mutiplicación
	#hacia el cliente (Emisor).
	"""

	def enviar_matriz_resultado(self, data, conexion):
		"""Aquí le decimos que por cada  cliente que hayamos aceptado en nuestrass conexiones
		va a verificar si efecticamente se trata del cliente y le mandamos el resultado, pero antes
		debemos serializarlo, esto gracias a pickle.dumps().
		"""
		for cliente in self.clientes:
			try:
				if cliente == conexion:
					matriz = pickle.dumps(data)
					print("Matriz C enviada:")
					print(data)
					cliente.send(matriz)
			except: 
				"""Si algo sale mal con alguno de los clientes será remocido del servidor"""
				self.clientes.remove(cliente)

	"""Gracias a esta función el servidor podrá aceptar las conexiones solicitadas por sus clientes (emisores)
	Esta está vínculada con el hilo aceptar, lo cual permite siempre estar atento hacia solicitudes
	"""

	def aceptarCon(self):
		print("aceptarCon iniciado")
		while True:
			try:
				conn, addr = self.sock.accept()
				conn.setblocking(False)
				self.clientes.append(conn)
			except:
				pass
	
	"""Al igual que la función aceptarCon, esta función está vinculada a un hilo, lo cual permite al servidor
	siempre mantenerse atento hacia las solicitudes de proceso (multiplicación de matrices) de los clientes.
	Aquí es donde recibimos las matrices enviadas para luego multiplicarlas.
	"""

	def procesarCon(self):
		print("ProcesarCon iniciado")
		while True:
			"""Esto se ejecutará si es que existen clientes conectados y recibirá las matrices envidas(si es 
			que así fuera) por ello es esto siempre se mantiene activo. 
			"""
			if len(self.clientes) > 0:
				for cliente in self.clientes:
					try:
						"""Con esta línea de código recibimos las matrices desde el cliente"""
						data = cliente.recv(1024)
						"""Y si en efecto existes elementos enviados, entonces procederemos a realizar
						la multiplicación.
						"""
						if data:
							"""Gracias a pickle.loads reconstruimos las matrices serializadas
							union = pickle.loads(data)
							Separamos lo que mandamos unido anteriormente por le emisor (cliente).
							"""
							A = np.array(union[0])
							B = np.array(union[1])
							"""Lo mostramos en consola para comprobar lo recibido"""
							print("Matriz A recibida:")
							print(A)
							print("Matriz B recibida:")
							print(B)
							"""Creamos la matriz resultado a partir de las dimensiones de las otras matrices
							A.shape[0] equivaldría a "n" y B.shape[1] a "p" esto teniendo en cuenta el formato
							establecido en el ejemplo A(nxm) . B(mxp)
							"""
							C = np.zeros((A.shape[0], B.shape[1]), dtype=np.int32)

							"""Finalmente creamos el pool de threads para realizar la multíplicación paralela."""
							executor = ThreadPoolExecutor(max_workers=2)
							"""Esto ya es parte de la multiplicación de matrices donde indicamos que tareas va realizar
							#en paralelo estos hilos.
							"""
							for i in range(C.shape[0]):
								for j in range(C.shape[1]):
									"""Indicamos que cada tarea está vinculada a la función matrix_multiplication
									la cual realiza el cálculo de un elemento de la matriz resultado dependiendo
									de los parámetros enviados.
									"""
									executor.submit(MultiplicacionHilo.matrix_multiplication, A, B, C, i, j)
							executor.shutdown()
							self.enviar_matriz_resultado(C, cliente)
					except:
						pass

"""Creamos e iniciamos el servidor"""
s = Servidor()
