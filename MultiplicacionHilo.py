from random import randint

"""Esta es la función principal para la multiplicación de matrices 
la cual realiza el cálculo de la suma de los elementos multiplicados de una
fila y columna especificada por los parámetros. Esto es clave para
el cálculo de cada elemento de la matriz resultante.
Finalmente recordar que cada tarea del pool ejecutará esto de forma paralela.
"""
def matrix_multiplication(A, B, C, fila, columna):
	suma = 0
	for i in range(A.shape[0]):
		suma = suma + A[fila,i]*B[i,columna]
	C[fila,columna] = suma

"""Esto fue utilizado para pruebas sin interfaz del cliente, también genera matrices aleatorias."""
def generar_matriz(M):
	random = 0
	for i in range(M.shape[0]):
		for j in range(M.shape[1]):
			random = randint(0, 100)
			M[i,j] = random
