import socket
import threading
import sys
import pickle
import numpy as np
from tkinter import *
from random import randint

class Interfaz:
	"""Al iniciar ya establecemos el host a donde nos conectaremos y así mismmo el puerto por donde
	#se realizará la conexión
	"""
	def __init__(self, ventana, host="localhost", port=6666):
		"""Elementos de la interfaz"""
		self.text_varA = []
		self.entriesA = []
		self.text_varB = []
		self.entriesB = []
		self.text_varC = []
		self.entriesC = []
		self.ventana = ventana
		self.ventana.geometry("600x600")
		self.ventana.title("2021 - CDP - EP - Multiplicación de matrices")
		self.ventana.configure(background="light coral")
		self.label_dimensiones_matriz = Label(self.ventana, text="A(nxm) . B(mxp)", bg="light coral")
		self.label_ingrese_dimensiones = Label(self.ventana, text="Ingrese rango de las matrices", bg="light coral")
		self.label_dimensiones_matriz.place(x=25, y=20)
		self.label_ingrese_dimensiones.place(x=25, y=50)
		self.boton_configurar_dimensiones = Button(self.ventana, text="Ingresar", command=self.abrirventana2)
		self.boton_configurar_dimensiones.place(x =200, y = 50)
		self.label_matriz_A = Label(self.ventana, text="Matriz A", bg="light coral")
		self.label_matriz_B = Label(self.ventana, text="Matriz B", bg="light coral")
		self.label_matriz_A.place(x =100, y = 100)
		self.label_matriz_B.place(x =200, y = 100)
		self.autor_nombre = Label(self.ventana,text = "by Daniel Mendiguri", bg="light coral")
		self.autor_nombre.pack(side=RIGHT)
		"""Sockets
		Aquí creamos el socket para el emisor (Cliente).
		"""
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		"""establecemos la conexión"""
		self.sock.connect((str(host), int(port)))
		"""Esta es la matriz donde recibiremos la respuesta del servidor."""
		self.matriz_respuesta = []

		"""Ejecutamos un hilo de espera con el fin de que es emisor espere la respuesta del servidor.""" 

		recibir_matriz_respuesta = threading.Thread(target=self.recibir_matriz_respuesta)
		"""Indicamos que es un hilo demonio para que el hilo sea ejecutado en segundo plano a la espera de
		respuestas del servidor e iniciamos este hilo.
		"""
		recibir_matriz_respuesta.daemon = True
		recibir_matriz_respuesta.start()
	"""Esta es la función que recibirá el hilo para esperar la respuesta."""
	def recibir_matriz_respuesta(self):
		while True:
			try:
				"""Aquí almacenamos la data recibida"""
				data = self.sock.recv(1024)
				if data:
					"""Si efectivamente todo llegó bien
					entonces recontruimos la matriz serializada recibida
					gracias a pickle.loads()
					"""
					self.matriz_respuesta = pickle.loads(data)
					print(self.matriz_respuesta)
					self.crear_tabla_C()
			except:
				pass
	"""Con esta función enviamos al servidor las matrices que queremos que multiplique."""
	def enviar_matrices_a_b(self, A, B):
		"""Unimos las matrices para pasarle un solo elemento serializado gracias a pickle.dumps()"""
		union = [A,B]
		self.sock.send(pickle.dumps(union))

	"""Este es otro elemento de la interfaz donde recibimos las dimensiones de las matrices."""
	def abrirventana2(self):
		self.ventana_dimensiones = Toplevel()
		self.ventana_dimensiones.geometry("350x200+300+100")
		self.ventana_dimensiones.configure(background="light coral")
		label_A = Label(self.ventana_dimensiones,text = "A", bg="light coral", font=("Verdana",24))
		label_B = Label(self.ventana_dimensiones,text = "B", bg="light coral", font=("Verdana",24))
		self.entrada_n = Text(self.ventana_dimensiones,width=2, height=1)
		self.entrada_m = Text(self.ventana_dimensiones,width=2, height=1)
		self.entrada_m2 = Text(self.ventana_dimensiones,state="disabled",width=2, height=1)
		self.entrada_p = Text(self.ventana_dimensiones, width=2, height=1)
		label_A.place(x=20, y=25)
		self.entrada_n.place(x=60, y=20)
		self.entrada_m.place(x=60, y=50)
		label_B.place(x=100, y=25)
		self.entrada_m2.place(x=140, y=20)
		self.entrada_p.place(x=140, y=50)
		"""Redireccionamos a la función confirmar dimensiones donde se asignaran los valores recibidos
		y configuraciones respectivas para la interfaz.
		"""
		boton_aceptar = Button(self.ventana_dimensiones, text="Aceptar", command=self.confirmardimensiones)
		boton_aceptar.place(x=140, y=100)

	"""Funcion para asignar los valores ingresados en la ventana de dimensiones."""
	def confirmardimensiones(self):
		self.n = int(self.entrada_n.get("1.0","end-1c"))
		self.m = int(self.entrada_m.get("1.0","end-1c"))
		self.p = int(self.entrada_p.get("1.0","end-1c"))
		print(self.n,self.m,self.p)
		self.ventana_dimensiones.destroy()
		self.label_dimensiones_matriz['text'] = "A(" + str(self.n) + "x" + str(self.m) + ") . " + "B(" + str(self.m) + "x" + str(self.p) + ")"
		self.crear_tabla_A()
		self.crear_tabla_B()
		self.boton_enviar = Button(self.ventana, text="Enviar", command=self.obtener_matrices)
		self.boton_enviar.place(x=30, y=100)
		self.boton_random = Button(self.ventana, text="Random", command=self.generar_matrices)
		self.boton_random.place(x=350, y=50)
		self.label_matriz_A.place(x=70 + (self.m*15), y=100)
		self.label_matriz_B.place(x=130 + (self.m*30) + (self.p*15), y=100)
	
	"""Esto es para construir la matriz resultado recibida en la interfaz."""
	def crear_tabla_C(self):
		x2 = 0
		y2 = 0
		self.text_varC.clear()
		self.entriesC.clear()
		rows, cols = (self.n, self.p)
		true_space = 170 + (rows * 30)
		self.label_matriz_C = Label(self.ventana,text = "Matriz C",bg="light coral")
		self.label_matriz_C.place(x=80 + (rows*16), y=true_space - 30)
		for i in range(rows):
			self.text_varC.append([])
			self.entriesC.append([])
			for j in range(cols):
				"""self.text_varC[i].append(StringVar(value=str(matriz_respuesta[i][j])))"""
				self.text_varC[i].append(StringVar())
				self.text_varC[i][j].set(str(self.matriz_respuesta[i][j]))
				self.entriesC[i].append(Entry(self.ventana, textvariable=self.text_varC[i][j],width=5))
				self.entriesC[i][j].place(x=100 + x2, y=true_space + y2)
				x2 += 38
			y2 += 30
			x2 = 0
	
	"""Esto es para asignar valores de una matriz recibida a la matriz A correspondiente de la interfaz."""

	def set_tabla_A(self,M):
		rows, cols = (self.n, self.m)
		true_space = rows*140
		for i in range(rows):
			for j in range(cols):
				self.text_varA[i][j].set(str(M[i][j]))

	"""Esto es para asignar valores de una matriz recibida a la matriz B correspondiente de la interfaz."""

	def set_tabla_B(self,M):
		rows, cols = (self.m, self.p)
		true_space = rows*140
		for i in range(rows):
			for j in range(cols):
				self.text_varB[i][j].set(str(M[i][j]))

	"""Esto es para construir la matriz A en la interfaz con las dimensiones establecidas."""
	def crear_tabla_A(self):
		x2 = 0
		y2 = 0
		self.text_varA.clear()
		self.entriesA.clear()
		rows, cols = (self.n, self.m)
		for i in range(rows):
			self.text_varA.append([])
			self.entriesA.append([])
			for j in range(cols):
				self.text_varA[i].append(StringVar())
				self.entriesA[i].append(Entry(self.ventana, textvariable=self.text_varA[i][j], width=3))
				self.entriesA[i][j].place(x=100 + x2, y=140 + y2)
				x2 += 30
			y2 += 30
			x2 = 0
	
	"""Esto es para construir la matriz B en la interfaz con las dimensiones establecidas."""
	def crear_tabla_B(self):
		x2 = 0
		y2 = 0
		self.text_varB.clear()
		self.entriesB.clear()
		rows, cols = (self.m, self.p)
		true_space = 160 + (rows*30)
		for i in range(rows):
			self.text_varB.append([])
			self.entriesB.append([])
			for j in range(cols):
				self.text_varB[i].append(StringVar())
				self.entriesB[i].append(Entry(self.ventana, textvariable=self.text_varB[i][j], width=3))
				self.entriesB[i][j].place(x=true_space + x2, y=140 + y2)
				x2 += 30
			y2 += 30
			x2 = 0

	"""Esta función es utilizada al darle click a enviar para obtener la información dada en la interfaz
	para almacenarlas en matrices y enviarlas al servidor para el calculo correspondiente (para lo último
	usamos la función "enviar_matrices_a_b()").
	"""
	def obtener_matrices(self):
		A = []
		B = []
		for i in range(self.n):
			A.append([])
			for j in range(self.m):
				A[i].append(int(self.text_varA[i][j].get()))
		for i in range(self.m):
			B.append([])
			for j in range(self.p):
				B[i].append(int(self.text_varB[i][j].get()))

		self.enviar_matrices_a_b(A, B)
	
	"""Esta función es para generar matrices aleatorias al darle click al botón random de la interfaz"""
	def generar_matrices(self):
		A = []
		B = []
		random = 0
		for i in range(self.n):
			A.append([])
			for j in range(self.m):
				random = randint(0, 100)
				A[i].append(random)
		for i in range(self.m):
			B.append([])
			for j in range(self.p):
				random = randint(0, 100)
				B[i].append(random)
		self.set_tabla_A(A)
		self.set_tabla_B(B)

"""Creamos e iniciamos la interfaz con la ventana principal"""
ventana_principal = Tk()
calculadora_matrices = Interfaz(ventana_principal)
ventana_principal.mainloop()
